import React, { useState } from "react";

export default function EditPlayerForm({savePlayer, player}) {
    const [username, setUsername] = useState(player.username);
    const [email, setEmail] = useState(player.email);
    const [experience, setExperience] = useState(player.experience);
    const [level, setLevel] = useState(player.level);

    function handleSave(e) {
        e.preventDefault()

        savePlayer(player.id, username, email, experience, level);
    }

    return(
        <form onSubmit={handleSave} className="new-item-form">
            <div className="form-row">
                <label htmlFor="item">Edit Player</label>
                <input 
                    value={username} 
                    onChange={e => setUsername(e.target.value)} 
                    type="text" 
                    id="item"
                    placeholder="Username"  
                />
                <br />
                <input 
                    value={email} 
                    onChange={e => setEmail(e.target.value)} 
                    type="email" 
                    id="item"
                    placeholder="Email"  
                />
                <br />
                <input 
                    value={experience} 
                    onChange={e => setExperience(e.target.value)} 
                    type="text" 
                    id="item" 
                    placeholder="Experience." 
                />
                <br />
                <input 
                    value={level} 
                    onChange={e => setLevel(e.target.value)} 
                    type="text" 
                    id="item" 
                    placeholder="Level" 
                />
            </div>
            <br />
            <button type="submit" className="btn btn-success">Save</button>
        </form>
    )
}