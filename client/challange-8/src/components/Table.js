import React from "react";
import EditPlayerForm from "./EditPlayerForm";

export default function Table({players, editPlayer, savePlayer, deletePlayer, query}) {
    return(
        <table>
            <tbody>
                <tr>
                    <th>username</th>
                    <th>email</th>
                    <th>experience</th>
                    <th>level</th>
                    <th>action</th>
                </tr>
                {players.filter(
                    (player) =>
                        player.username.toLowerCase().includes(query) ||
                        player.email.toLowerCase().includes(query) ||
                        player.experience.toLowerCase().includes(query) ||
                        player.level.toLowerCase().includes(query)
                    ).map(player => {
                    return (
                        player.isEditing ? (
                            <div>
                                <EditPlayerForm savePlayer={savePlayer} player={player} />
                            </div>
                        ) : (
                            <tr key={player.id}>
                                <td>{player.username}</td>
                                <td>{player.email}</td>
                                <td>{player.experience}</td>
                                <td>{player.level}</td>
                                <td>
                                    <button
                                        onClick={() => editPlayer(player.id)} 
                                        className="btn btn-success">
                                        Edit
                                    </button>
                                    <span></span>
                                    <button 
                                        onClick={() => deletePlayer(player.id)} 
                                        className="btn btn-danger">
                                        X
                                    </button>
                                </td>
                            </tr>
                        )
                    )
                })}
            </tbody>
      </table>
    )
}