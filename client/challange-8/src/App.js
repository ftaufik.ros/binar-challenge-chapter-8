import React, { useState } from 'react'
import NewPlayerForm from './components/NewPlayerForm';
import SearchInput from './components/SearchInput';
import Table from './components/Table';
import './style.css';

function App() {
  const [players, setPlayers] = useState([]);
  const [query, setQuery] = useState("");

  function addPlayer(username, email, experience, level) {
    setPlayers(currentPlayers => {
      return [
        ...currentPlayers,
        {
          id: players.length + 1,
          username,
          email,
          experience,
          level,
          isEditing: false
        },
      ]
    })
  }

  function editPlayer(id) {
    setPlayers(
      players.map((player) =>
        player.id === id ? { ...player, isEditing: !player.isEditing } : player
      )
    );
  }

  function savePlayer(id, username, email, experience, level) {
    setPlayers(
      players.map((player) =>
        player.id === id ? 
          { 
            ...player, 
            username, 
            email, 
            experience, 
            level, 
            isEditing: !player.isEditing 
          } : player
      )
    );

    setQuery("")
  }

  function deletePlayer(id) {
    setPlayers(currentPlayer => {
      return currentPlayer.filter(player => player.id !== id)
    })

    setQuery("")
  }

  return (
    <>
      <SearchInput 
        query={query} 
        setQuery={setQuery} 
      />

      <br />
      <hr />
      <br />

      <NewPlayerForm 
        onSubmit={addPlayer} 
      />

      <Table 
        players={players}
        editPlayer={editPlayer}
        savePlayer={savePlayer}
        deletePlayer={deletePlayer}
        query={query}
      />
    </>
  );
}

export default App;
